package br.com.samples.loadimagepicasso;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        imageView = findViewById(R.id.iv_clinica);

        Picasso.with(this)
                .load("https://pouppe.websiteseguro.com/evosaude/sistema/fotos/a2163266e92b84644aaee0749d833378/Logo_ArnaldoGRD.jpg")
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        imageView.setImageResource(R.drawable.ic_error_black_24dp);
                    }
                });
    }

}
